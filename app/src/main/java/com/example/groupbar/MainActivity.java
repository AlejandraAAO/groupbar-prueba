package com.example.groupbar;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    BarChart mpBarChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mpBarChart = findViewById(R.id.groupBarChart);
        BarDataSet barDataSet1 = new BarDataSet(barEntries(),"DataSet1");
        BarDataSet barDataSet2 = new BarDataSet(barEntries2(),"DataSet2");
        BarDataSet barDataSet3 = new BarDataSet(barEntries3(),"DataSet3");
        BarDataSet barDataSet4 = new BarDataSet(barEntries4(),"DataSet4");

        barDataSet1.setColor(Color.RED);
        barDataSet2.setColor(Color.MAGENTA);
        barDataSet3.setColor(Color.DKGRAY);
        barDataSet4.setColor(Color.YELLOW);

        BarData data = new BarData(barDataSet1,barDataSet2,barDataSet3,barDataSet4);
        mpBarChart.setData(data);

        //formateando el eje x
        String[] days = new String[]{"Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"};
        XAxis xAxis = mpBarChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(days));
        xAxis.setCenterAxisLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1);
        xAxis.setGranularityEnabled(true);

        mpBarChart.setDragEnabled(true);
        mpBarChart.setVisibleXRangeMaximum(3);

        float barSpace = 0.08f;
        float groupSpace = 0.44f;
        data.setBarWidth(0.10f);

        mpBarChart.getXAxis().setAxisMinimum(0);
        mpBarChart.getXAxis().setAxisMaximum(0+mpBarChart.getBarData().getGroupWidth(groupSpace,barSpace)*7);
        mpBarChart.getAxisLeft().setAxisMinimum(0);
        mpBarChart.groupBars(0,groupSpace,barSpace);
        mpBarChart.invalidate();
    }

    private ArrayList<BarEntry> barEntries()
    {
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1,2000));
        barEntries.add(new BarEntry(2,791));
        barEntries.add(new BarEntry(3,630));
        barEntries.add(new BarEntry(4,458));
        barEntries.add(new BarEntry(5,2724));
        barEntries.add(new BarEntry(6,500));
        barEntries.add(new BarEntry(7,2173));

        return barEntries;
    }

    private ArrayList<BarEntry> barEntries2()
    {
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1,900));
        barEntries.add(new BarEntry(2,631));
        barEntries.add(new BarEntry(3,1040));
        barEntries.add(new BarEntry(4,382));
        barEntries.add(new BarEntry(5,2614));
        barEntries.add(new BarEntry(6,5000));
        barEntries.add(new BarEntry(7,2358));

        return barEntries;
    }
    private ArrayList<BarEntry> barEntries3()
    {
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1,1230));
        barEntries.add(new BarEntry(2,3631));
        barEntries.add(new BarEntry(3,5040));
        barEntries.add(new BarEntry(4,6952));
        barEntries.add(new BarEntry(5,6878));
        barEntries.add(new BarEntry(6,4850));
        barEntries.add(new BarEntry(7,3650));

        return barEntries;
    }
    private ArrayList<BarEntry> barEntries4()
    {
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1,3516));
        barEntries.add(new BarEntry(2,463));
        barEntries.add(new BarEntry(3,504));
        barEntries.add(new BarEntry(4,600));
        barEntries.add(new BarEntry(5,1600));
        barEntries.add(new BarEntry(6,4000));
        barEntries.add(new BarEntry(7,879));

        return barEntries;
    }
}
